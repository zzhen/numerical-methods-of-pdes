close all
clear all

rho = 1000;
cs = 2;
csh = 2;
csv = 6; % csv = 2;
cp = 20;
alpha = cp;
beta = cs;
miu = cs^2*rho;
lambda = cp^2*rho-2*miu;


% rho = 1000;
% cs = 2; % shear wave speed
% csh = 3;
% csv = 6;
% cp = 50;  % pressure wave speed
% alpha = cp;
% beta = cs;
% miu = cs^2*rho;
% lambda = cp^2*rho-2*miu;

%Isotropic
% c11 = (lambda+2*miu)/rho;%42.6;%(lambda+4*miu)/rho;%ET;%(lambda+2*miu)/rho;%42.6;%80.6;
% c33 = (lambda+2*miu)/rho;%34.6;%(lambda+2*miu)/rho;%EL;%(lambda+2*miu)/rho;%;34.6*2;
% c12 = lambda/rho;%25.4;%lambda/rho;%25.4;
% c13 = lambda/rho;%19.7;%lambda/rho;%EL/niuL;%lambda/rho;%;19.7;
% c44 = miu/rho;%9.4;%miu/rho;%miuL;%miu/rho;%;9.4;
% c66 = miu/rho;%(c11-c12)/2;%miu/rho;%8.6;

%Anisotropic
    c11 = cp*cp;
    c33 = c11;
    c44 = csh*csh;
    c66 = csv*csv;
    c12 = c11*c11-c66;
    c13 = sqrt(c33*(c11-c66));

%Apatite
% c11 = 16.7;
% c33 = 14.0;
% c44 = 6.63;
% c66 = csv^2;
% c12 = rho*c11^2-2*c66;
% c13 = 6.6;

%Zinc
% c11 = 16.5;
% c33 = 6.2;
% c44 = 3.69;
% c66 = csv^2;
% c12 = rho*c11^2-2*c66;
% c13 = 5;

%Cobalt
% c11 = 30.7;
% c33 = 35.8;
% c44 = 7.55;
% c66 = csv^2;
% c12 = rho*c11^2-2*c66;
% c13 = 10.3;

% c11 = cp^2;
% c33 = cp^2;
% c44 = csh^2;
% c66 = csv^2;
% c12 = rho*c11^2-2*c66;
% c13 = sqrt(c33*(c11));

% 
% miuL = 25.0e3;
% miuT = 9e3;
% ET = 32.76e3;
% EL = 47.25e3;
% niuL = 0.499;
% niuT = 0.68;
% 
% S = [1/ET -niuT/ET -niuL/EL 0 0 0; -niuT/ET 1/ET -niuL/EL 0 0 0; -niuL/EL -niuL/EL 1/EL 0 0 0;...
%     0 0 0 1/miuL 0 0; 0 0 0 0 1/miuL 0; 0 0 0 0 0 1/miuT];
% C = inv(S)/rho;
% 
% c11 = C(1,1);
% c12 = C(1,2);
% c13 = C(1,3);
% c33 = C(3,3);
% c44 = C(4,4);
% c66 = C(6,6);


% cpt = sqrt(c11);
% cpl = sqrt(c33);
% csl = sqrt(c44);
% cst = sqrt(c66);

cpt = cp;
cpl = cp;
csl = cs;
cst = cs;

Nt = 3000;   % time sample
tzone = 10e-3;  % 10 ms
t = [1:Nt]/Nt*tzone;
dt = t(2)-t(1);
fs = 1/dt;

Nx = 201;
xzone = 50e-3;
x = [-(Nx-1)/2:(Nx-1)/2]/((Nx-1)/2)*xzone;  %-50 - 50 mm
dx = x(2)-x(1);

Ny = Nx;
yzone = xzone;
y = x;
dy = dx;

F = cp*dt/dx;
gama = cs/cp;
fprintf(['dx/dt=',num2str(dx/dt),'.\n']);

W = 2000e-6;
D = 000e-6;
rectinput = heaviside(t-D)-heaviside(t-W-D);

% tc = gauspuls('cutoff',2e5/80,0.6,[],-40);
% tt = -tc : dt : tc;
% tc = gauspuls(tt,2e5/80,0.6);

tc = sin((t-D)*(pi/W)).*rectinput;
% tc = sin((t-D)*(pi/W));
% tc = rectinput;

inc = zeros(1,Nt);
inc(1:length(tc)) = tc;

% inc = rectinput;

% u=zeros(Nx,Ny);%iteration mat
% upp=u;
% up=u;
u=zeros(Nx,Ny,Nt);  %ux
w=zeros(Nx,Ny,Nt);  %uz

tic;
for l=2:Nt-1
    if l==10||l==20||l==30||l==50||l==100||l==200||l==300
        if max(max(abs(u(:,:,l))))>1e5;
            error('diverge!');
        end
    end
    timeused = toc;
    estimated_time = timeused*(Nt-l)/(l-2);
    fprintf([num2str(l),'/',num2str(Nt),', will be finished in: ',num2str(estimated_time),'s.','\n'])
    
%     u((Ny+1)/2,(Ny+1)/2,l)=u((Ny+1)/2,(Ny+1)/2,l)+inc(l);
    w((Ny+1)/2,(Ny+1)/2,l)=w((Ny+1)/2,(Ny+1)/2,l)+inc(l);
    
    for m=2:1:Nx-1
        
        for n=2:1:Nx-1
            
%             %x-z plane
            u(m,n,l+1)=2*u(m,n,l) - u(m,n,l-1) + c11*(dt/dx)^2*(u(m+1,n,l)-2*u(m,n,l)+u(m-1,n,l))+...
                (c13+c44)*(dt/dx)^2*(w(m+1,n+1,l)-w(m+1,n-1,l)-w(m-1,n+1,l)+w(m-1,n-1,l))/4+...
                c44*(dt/dx)^2*(u(m,n+1,l)-2*u(m,n,l)+u(m,n-1,l));
            
            
            w(m,n,l+1)=2*w(m,n,l) - w(m,n,l-1) + c33*(dt/dx)^2*(w(m,n+1,l)-2*w(m,n,l)+w(m,n-1,l))+...
                (c13+c44)*(dt/dx)^2*(u(m+1,n+1,l)-u(m+1,n-1,l)-u(m-1,n+1,l)+u(m-1,n-1,l))/4+...
                c44*(dt/dx)^2*(w(m+1,n,l)-2*w(m,n,l)+w(m-1,n,l));
%             %x-z plane
            
            %x-y plane
%             u(m,n,l+1)=2*u(m,n,l) - u(m,n,l-1) + c11*(dt/dx)^2*(u(m+1,n,l)-2*u(m,n,l)+u(m-1,n,l))+...
%                 (c12+c66)*(dt/dx)^2*(w(m+1,n+1,l)-w(m+1,n-1,l)-w(m-1,n+1,l)+w(m-1,n-1,l))/4+...
%                 c66*(dt/dx)^2*(u(m,n+1,l)-2*u(m,n,l)+u(m,n-1,l));
%             
%             w(m,n,l+1)=2*w(m,n,l) - w(m,n,l-1) + c11*(dt/dx)^2*(w(m,n+1,l)-2*w(m,n,l)+w(m,n-1,l))+...
%                 (c12+c66)*(dt/dx)^2*(u(m+1,n+1,l)-u(m+1,n-1,l)-u(m-1,n+1,l)+u(m-1,n-1,l))/4+...
%                 c66*(dt/dx)^2*(w(m+1,n,l)-2*w(m,n,l)+w(m-1,n,l));
            %x-y plane
            
            
            %         w(m,n,l+1)=2*w(m,n,l) - w(m,n,l-1) + c33*(dt/dx)^2*(w(m,n+1,l)-2*w(m,n,l)+w(m,n-1,l))+...
            %             (c13)*(dt/dx)^2*(u(m+1,n+1,l)-u(m+1,n-1,l)-u(m-1,n+1,l)+u(m-1,n-1,l))/4+...
            %             c44*(dt/dx)^2*(w(m+1,n,l)-2*w(m,n,l)+w(m-1,n,l));
            
            
%             if (m==(Ny+1)/2)&&(n==(Ny+1)/2)&&(l<Nt)
%                 w(m,n,l)=w(m,n,l)+inc(l);
%                 %             w(m,n,l)=w(m,n,l)+rectinput(l+1)-rectinput(l);
%             end
            
        end
        
        %BC
        %     j=1;
        %     for i=2:1:gd-1
        %         Ptx=(Mp(i,j)-Mpp(i,j)+Mp(i-1,j)-Mpp(i-1,j))/(dx*dt);%dPai/(dx*dt)
        %         Pyy=(Mp(i,j+1)-2*Mp(i,j)+Mp(i,j+1))/(dy^2);
        %         M(i,1)=(C(i,j)/2*Pyy-Ptx)*C(i,j)*dt^2+2*Mp(i,j)-Mpp(i,j);
        %     end
        
        
        
        
        %    pause(0.05);
        %    plot(M(:,n));
    end
    
    %BC
    n=1;
    for m=2:1:Nx-1
        
        %         utz=(-u(m,n,l)+u(m,n,l-1)+u(m,n+1,l)-u(m,n+1,l-1))/(dy*dt);%dPai/(dx*dt)
        %         uxx=(u(m+1,n,l)-2*u(m,n,l)+u(m-1,n,l))/(dx^2);
        %         wtx=(w(m+1,n,l)-w(m+1,n,l-1)-w(m,n,l)+w(m,n,l-1))/(dx*dt);%dPai/(dx*dt)
        %         RHS=utz-(beta-alpha)/beta*wtx-(beta-2*alpha)/2*uxx;
        %         u(m,n,l+1)=RHS*dt^2*beta+2*u(m,n,l)-u(m,n,l-1);
        %
        %         wtz=(-w(m,n,l)+w(m,n,l-1)+w(m,n+1,l)-w(m,n+1,l-1))/(dy*dt);%dPai/(dx*dt)
        %         wxx=(w(m+1,n,l)-2*w(m,n,l)+w(m-1,n,l))/(dx^2);
        %         utx=(u(m+1,n,l)-u(m+1,n,l-1)-u(m,n,l)+u(m,n,l-1))/(dx*dt);%dPai/(dx*dt)
        %         RHS=-wtz+(beta-alpha)/alpha*utx+(alpha-2*beta)/2*wxx;
        %         w(m,n,l+1)=-RHS*dt^2*alpha+2*w(m,n,l)-w(m,n,l-1);
        
        uz = (u(m,n+1,l)-u(m,n,l))/dy;
        u(m,n,l+1) = beta*dt*(uz) + u(m,n,l);%beta
        
        wz = (w(m,n+1,l)-w(m,n,l))/dy;
        w(m,n,l+1) = alpha*dt*(wz) + w(m,n,l);%alpha
    end
    
    n=Nx;
    for m=2:1:Nx-1
        uz = (u(m,n,l)-u(m,n-1,l))/dy;
        u(m,n,l+1) = -beta*dt*(uz) + u(m,n,l);
        
        wz = (w(m,n,l)-w(m,n-1,l))/dy;
        w(m,n,l+1) = -alpha*dt*(wz) + w(m,n,l);
    end
    
    m=1;
    for n=1:1:Nx
        uz = (u(m+1,n,l)-u(m,n,l))/dy;
        u(m,n,l+1) = alpha*dt*(uz) + u(m,n,l);
        
        wz = (w(m+1,n,l)-w(m,n,l))/dy;
        w(m,n,l+1) = beta*dt*(wz) + w(m,n,l);
    end
    
    m=Nx;
    for n=1:1:Nx
        uz = (u(m,n,l)-u(m-1,n,l))/dy;
        u(m,n,l+1) = -alpha*dt*(uz) + u(m,n,l);
        
        wz = (w(m,n,l)-w(m-1,n,l))/dy;
        w(m,n,l+1) = -beta*dt*(wz) + w(m,n,l);
    end
    
    
    
    
    
    %
    %     i=1;j=1;
    %     M(i,j)=c*dt/dx*(Mp(i,j+1)-Mp(i,j))+Mp(i,j);
    %     i=gd;j=1;
    %     M(i,j)=c*dt/dx*(Mp(i,j+1)-Mp(i,j))+Mp(i,j);
    %     i=1;j=gd;
    %     M(i,j)=-c*dt/dx*(Mp(i,j)-Mp(i,j-1))+Mp(i,j);
    %     i=gd;j=gd;
    %     M(i,j)=-c*dt/dx*(Mp(i,j)-Mp(i,j-1))+Mp(i,j);
    
    
    
    %     j=1;
    %     for i=2:1:gd-1
    %         M(i,j)=-C(i,j)/dx*(Mp(i,j)-Mp(i,j+1))+Mp(i,j);
    %     end
    
    %     umat(it,:,:)=u;
    
    %     figure(3)
    %     %mesh(M/max(max(M)))
    %     mesh(M)
    %     view(0,90)
    %     %axis('equal')
    %     %colorbar;
    %     %zlim([-1 1])
    %     %caxis([-1 1])
    %     upp=up;
    %     up=u;
end
toc;
% unow=u(:,:,120);



% clear f im map A
% filename = 'test.gif';

figure(7)
tmes = 20e-3;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)



clear f
figure(3)
for d=1:10:Nt
    %     figure(1)
    
    unow=w(:,:,d)';
    uplot=reshape(unow,Nx,Nx);
    
    mesh(uplot);
    colorbar;
%     zlim([-0.5 0.5])
    view(0,90)
    axis equal;
    title(num2str(d))
    
    pause(0.05);
    %     f=getframe(1);
    %     imind=frame2im(f);
    %     [imind,cm] = rgb2ind(imind,256);
    %     if d==1
    %         imwrite(imind,cm,'Temp1.gif','gif', 'Loopcount',inf,'DelayTime',0.3);
    % %         [animated, cmap] = rgb2ind(f.cdata, 256, 'nodither');
    %     else
    %         imwrite(imind,cm,'Temp1.gif','gif','WriteMode','append','DelayTime',0.01);
    % %         animated(:,:,1,i) = rgb2ind(f.cdata, cmap, 'nodither');
    %     end
    %     close
    
end



% 
% figure(4)
% for dis = 1:round((Ny+1)/8)
%     
%     ptest = (squeeze(w((Ny+1)/2,(Ny+1)/2+dis,:)))/120;
%     pint = zeros(size(ptest));
%     %     for k=2:length(ptest)
%     %         pint(k)=trapz(t(1:k),ptest(1:k));
%     %     end
%     %     plot(t,pint)
%     %     hold on;
%     plot(t,ptest,'r')
%     hold off;
%     title(['FDTD,rect input, x=',num2str(x(dis+(Ny+1)/2)*1000),'mm'])
%     xlabel('t(s)')
%     
%     pause(0.005)
%     
% end





figure(9)
subplot(2,3,1)
tmes = 20e-5;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
% colorbar;
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)


subplot(2,3,2)
tmes = 20e-4;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)
% colorbar;

subplot(2,3,3)
tmes = 40e-4;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)
% colorbar;

subplot(2,3,4)
tmes = 60e-4;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)
% colorbar;

subplot(2,3,5)
tmes = 80e-4;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)
% colorbar;


subplot(2,3,6)
tmes = 15e-3;
[~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w(:,:,tdd-1)');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
xlabel('x(mm)' ,'FontSize', 16)
ylabel('z(mm)' ,'FontSize', 16)
% colorbar;