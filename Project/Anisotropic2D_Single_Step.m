% close all
clear all

rho = 1000;
cs = 2;
csh = 2;
csv = 2;
cp = 1500;
alpha = cp;
beta = cs;
miu = cs^2*rho;
lambda = cp^2*rho-2*miu;

type = 0; %0 = isotropic, 1 = anisotropic

if type==0
    medium_status = 'Isotropic';
    c11 = (lambda+2*miu)/rho;
    c33 = (lambda+2*miu)/rho;%
    c13 = lambda/rho;
    c44 = miu/rho;
elseif type==1
    medium_status = 'Anisotropic'; 
    
%     c11 = (lambda+2*miu)/rho;%ET;%(lambda+2*miu)/rho;%42.6;%80.6;
%     c33 = (lambda+2*miu)/rho;%EL;%(lambda+2*miu)/rho;%;34.6*2;
%     c12 = 25.4;
%     c13 = lambda/rho;%EL/niuL;%lambda/rho;%;19.7;
%     c44 = miu/rho;%miuL;%miu/rho;%;9.4;
%     c66 = 8.6;

        c11 = 16.2;%cp^2;
        c33 = 6.2;%cp^2;
        c44 = 3.69;%csh^2;
        c66 = csv^2*1000;
        c12 = c11^2-2*c66;
        c13 = 5;%sqrt(c33*(c11-c66));        
end

Nt = 300000;
tzone = 10e-3;
t = [1:Nt]/Nt*tzone;
dt = t(2)-t(1);
fs = 1/dt;

Nx = 401;
xzone = 50e-3;
x = [-(Nx-1)/2:(Nx-1)/2]/((Nx-1)/2)*xzone;
dx = x(2)-x(1);

Ny = Nx;
yzone = xzone;
y = x;
dy = dx;

F = cp*dt/dx;
gama = cs/cp;

fprintf(['dx/dt=',num2str(dx/dt),'.\n']);

W=2000e-6;
D=000e-6;
rectinput=heaviside(t-D)-heaviside(t-W-D);

tc = gauspuls('cutoff',2e5/80,0.6,[],-40);
tt = -tc : dt : tc;
tc = gauspuls(tt,2e5/80,0.6);

tc = sin((t-D)*(pi/W)).*rectinput;
% tc = sin((t-D)*(pi/W));
% tc = rectinput;

inc=zeros(1,Nt);
inc(1:length(tc))=tc;

% inc = rectinput;

% u=zeros(Nx,Ny);%iteration mat
% upp=u;
% up=u;
u=zeros(Nx,Ny);
w=zeros(Nx,Ny);
up=zeros(Nx,Ny);
wp=zeros(Nx,Ny);
upp=zeros(Nx,Ny);
wpp=zeros(Nx,Ny);

tic;
ltimeused = 0;
for l=2:Nt-1
    
    if l==10||l==20||l==30||l==50||l==100||l==200||l==300
        if max(max(abs(u)))>1e4;
            error('Diverge!');
        end
    end
    ltimeused = toc;
    estimated_time = ltimeused*(Nt-l)/(l-2);
    fprintf([num2str(l),'/',num2str(Nt),', will be finished in: ',num2str(estimated_time),'s.','\n'])
    
    wp((Ny+1)/2,(Ny+1)/2) = wp((Ny+1)/2,(Ny+1)/2)+inc(l);
%     wp(26,35);
    
    for m=2:1:Nx-1
        
        for n=2:1:Nx-1
            
            % x-z plane
            u(m,n)=2*up(m,n) - upp(m,n) + c11*(dt/dx)^2*(up(m+1,n)-2*up(m,n)+up(m-1,n))+...
                (c13+c44)*(dt/dx)^2*(wp(m+1,n+1)-wp(m+1,n-1)-wp(m-1,n+1)+wp(m-1,n-1))/4+...
                c44*(dt/dx)^2*(up(m,n+1)-2*up(m,n)+up(m,n-1));
            
            w(m,n)=2*wp(m,n) - wpp(m,n) + c33*(dt/dx)^2*(wp(m,n+1)-2*wp(m,n)+wp(m,n-1))+...
                (c13+c44)*(dt/dx)^2*(up(m+1,n+1)-up(m+1,n-1)-up(m-1,n+1)+up(m-1,n-1))/4+...
                c44*(dt/dx)^2*(wp(m+1,n)-2*wp(m,n)+wp(m-1,n));
            
            % x-y plane
            %         w(m,n)=2*wp(m,n) - wpp(m,n) + c33*(dt/dx)^2*(wp(m,n+1)-2*wp(m,n)+wp(m,n-1))+...
            %             (c13)*(dt/dx)^2*(up(m+1,n+1)-up(m+1,n-1)-up(m-1,n+1)+up(m-1,n-1))/4+...
            %             c44*(dt/dx)^2*(wp(m+1,n)-2*wp(m,n)+wp(m-1,n));
            
            
%             if (m==(Ny+1)/2)&&(n==(Ny+1)/2)&&(l<Nt)
%                 wp(m,n)=wp(m,n)+inc(l);
                %             wp(m,n)=wp(m,n)+rectinput(l+1)-rectinput(l);
%             end
            
        end
        

    end
    
    % ************** absorption boundary condition
    n=1;
    for m=2:1:Nx-1
        
        %         utz=(-up(m,n)+upp(m,n)+up(m,n+1)-u(m,n+1,l-1))/(dy*dt);%dPai/(dx*dt)
        %         uxx=(up(m+1,n)-2*up(m,n)+up(m-1,n))/(dx^2);
        %         wtx=(wp(m+1,n)-w(m+1,n,l-1)-wp(m,n)+wpp(m,n))/(dx*dt);%dPai/(dx*dt)
        %         RHS=utz-(beta-alpha)/beta*wtx-(beta-2*alpha)/2*uxx;
        %         u(m,n)=RHS*dt^2*beta+2*up(m,n)-upp(m,n);
        %
        %         wtz=(-wp(m,n)+wpp(m,n)+wp(m,n+1)-w(m,n+1,l-1))/(dy*dt);%dPai/(dx*dt)
        %         wxx=(wp(m+1,n)-2*wp(m,n)+wp(m-1,n))/(dx^2);
        %         utx=(up(m+1,n)-u(m+1,n,l-1)-up(m,n)+upp(m,n))/(dx*dt);%dPai/(dx*dt)
        %         RHS=-wtz+(beta-alpha)/alpha*utx+(alpha-2*beta)/2*wxx;
        %         w(m,n)=-RHS*dt^2*alpha+2*wp(m,n)-wpp(m,n);
        
        uz = (up(m,n+1)-up(m,n))/dy;
        u(m,n) = beta*dt*(uz) + up(m,n);
        
        wz = (wp(m,n+1)-wp(m,n))/dy;
        w(m,n) = alpha*dt*(wz) + wp(m,n);
    end
    
    n=Nx;
    for m=2:1:Nx-1
        uz = (up(m,n)-up(m,n-1))/dy;
        u(m,n) = -beta*dt*(uz) + up(m,n);
        
        wz = (wp(m,n)-wp(m,n-1))/dy;
        w(m,n) = -alpha*dt*(wz) + wp(m,n);
    end
    
    m=1;
    for n=1:1:Nx
        uz = (up(m+1,n)-up(m,n))/dy;
        u(m,n) = beta*dt*(uz) + up(m,n);
        
        wz = (wp(m+1,n)-wp(m,n))/dy;
        w(m,n) = alpha*dt*(wz) + wp(m,n);
    end
    
    m=Nx;
    for n=1:1:Nx
        uz = (up(m,n)-up(m-1,n))/dy;
        u(m,n) = -beta*dt*(uz) + up(m,n);
        
        wz = (wp(m,n)-wp(m-1,n))/dy;
        w(m,n) = -alpha*dt*(wz) + wp(m,n);
    end
    
    
    
    
    
    %
    %     i=1;j=1;
    %     M(i,j)=c*dt/dx*(Mp(i,j+1)-Mp(i,j))+Mp(i,j);
    %     i=gd;j=1;
    %     M(i,j)=c*dt/dx*(Mp(i,j+1)-Mp(i,j))+Mp(i,j);
    %     i=1;j=gd;
    %     M(i,j)=-c*dt/dx*(Mp(i,j)-Mp(i,j-1))+Mp(i,j);
    %     i=gd;j=gd;
    %     M(i,j)=-c*dt/dx*(Mp(i,j)-Mp(i,j-1))+Mp(i,j);
    
    
    
    %     j=1;
    %     for i=2:1:gd-1
    %         M(i,j)=-C(i,j)/dx*(Mp(i,j)-Mp(i,j+1))+Mp(i,j);
    %     end
    
    %     umat(it,:,:)=u;
    
    %     figure(3)
    %     %mesh(M/max(max(M)))
    %     mesh(M)
    %     view(0,90)
    %     %axis('equal')
    %     %colorbar;
    %     %zlim([-1 1])
    %     %caxis([-1 1])
    upp=up;
    up=u;
    wpp=wp;
    wp=w;
end
toc;
% unow=u(:,:,120);



% if type==0
%     save(['Isotropic_2D_cs=',num2str(cs),'_cp=',num2str(cp),'_',num2str(Nt),'of',num2str(tzone*1e3),...
%         'ms_',num2str(Nx),'of',num2str(xzone*1e3),'mm.mat'])
% elseif type==1
%     save(['Anisotropic_2D_cs=',num2str(cs),'_cp=',num2str(cp),'_',num2str(Nt),'of',num2str(tzone*1e3),...
%         'ms_',num2str(Nx),'of',num2str(xzone*1e3),'mm.mat'])
% end


figure(8)
tmes = 20e-3;
% [~,tdd]=min(abs(t-tmes));
imagesc(x*1e3,y*1e3,w');
set(gca,'FontSize',16)
axis equal;
axis tight;
% set(gcf,'Position',[200 200 500 500]);
set(gca,'XTick', min(x*1e3):20:max(x*1e3));
set(gca,'YTick', min(y*1e3):20:max(y*1e3));
title(['cp=',num2str(cp)])
xlabel('x(mm)' ,'FontSize', 16)
ylabel('y(mm)' ,'FontSize', 16)



% clear f
% figure(3)
% for d=1:10:Nt
% %     figure(1)
%
%     unow=-w(:,:,d);
%     uplot=reshape(unow,Nx,Nx);
%
%     mesh(uplot');
%     view(0,90)
%     axis equal;
%     title(num2str(d))
%
%     pause(0.05);
% %     f=getframe(1);
% %     imind=frame2im(f);
% %     [imind,cm] = rgb2ind(imind,256);
% %     if d==1
% %         imwrite(imind,cm,'Temp1.gif','gif', 'Loopcount',inf,'DelayTime',0.3);
% % %         [animated, cmap] = rgb2ind(f.cdata, 256, 'nodither');
% %     else
% %         imwrite(imind,cm,'Temp1.gif','gif','WriteMode','append','DelayTime',0.01);
% % %         animated(:,:,1,i) = rgb2ind(f.cdata, cmap, 'nodither');
% %     end
% %     close
%
% end




% figure(4)
% for dis = 1:round((Ny+1)/8)
%
%     ptest = -(squeeze(w((Ny+1)/2,(Ny+1)/2+dis)))/120;
%     pint = zeros(size(ptest));
% %     for k=2:length(ptest)
% %         pint(k)=trapz(t(1:k),ptest(1:k));
% %     end
% %     plot(t,pint)
% %     hold on;
%     plot(t,ptest,'r')
%     hold off;
%     title(['FDTD,rect input, x=',num2str(x(dis+(Ny+1)/2)*1000),'mm'])
%     xlabel('t(s)')
%
%     pause(0.005)
%
% end







