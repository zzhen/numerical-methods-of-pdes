# -*- coding: utf-8 -*-
"""
Created on Sat Nov  2 22:06:33 2013

@author: mspieg

Revised by zzhen
First, this code create a new class类: stupidode;
Following "set_integrator, set_initial_value(), etc." are member functions, which
means the attributs and methods owned by the objects/instances对象 in this class
And then at the lower bottom "ode = stupidode(func)" , this means that stupidode(**) creates 
a object/instance, i.e. "ode";
"""

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab as pyl
import matplotlib.pyplot as plt
from scipy.optimize import fsolve


class stupidode:
    """ simple wrapper class for hand rolled single stepper schemes that mimic
    the scipy ode class:
    
    Usage:  ode = stupidode(f)  # initialize an ode solver with rhs f(t,y)
            ode.set_integrator(method,nsteps)
            ode.set_initial_value(y0,t0)
            ode.integrate(t_max)
    """
    def __init__(self,f): # called when object is constructed
        """Initialize the ode object and set the rhs f
        """
        #self is a class, which owns the value 'f'
        self.f=f
        self.y0 = np.array([0])      
    def set_integrator(self,method,nsteps=1):
        """ set the integration scheme:
        method is the stepper (currently only euler)
        nsteps is the number of fixed sized steps taken over an integration
        """
        self.method = method
        self.nsteps = nsteps
    def set_initial_value(self,y0,t0):
        """ set the initial condition y0,t0
        """
        self.y0 = y0.copy()
        self.t0 = t0
        self.y = y0
        self.t = t0
        
    def integrate(self,t):
        """take nstep steps from t0 to t (ugly!) 
        given by ode.integrate(1.)
        """
        #nsteps is give by ode.set_integrator('euler',nsteps=**)
        nsteps = self.nsteps
        #print 'nsteps', nsteps
        #t is given by ode.integrate(1)
        self.tn = np.linspace(self.t0,t,nsteps+1)
        k  = np.diff(self.tn) #because tn has been defined in this method, so 'k'
                             #hasn't been added by 'self.'
                             #len(k) = len(tn)-1
        #print'k', k
        f = self.f  #set the value of f as it in the __init__(self,f)
                        #It's definition is "func(t,u), returns two values.
        self.X = []  #Stores values of x axis 
        self.Y = []  #Stores values of y axis
        self.u0 = []  #Stores values of the 1st col in rhs
        self.u1 = []  #Stores values of the 2nd col in rhs
        self.psi = []  #Sores values of streamfunction at each step        
        psi_o = psi(self.y)  #Given the value of psi_0
        self.r_error = [0]  #Stores values of relative errors
        self.psi_func = [psi_o]  #Sores values of streamfunction at all steps 
        for i in xrange(nsteps):
            #print'i, k', i, k[i] 
            #print'self.y', self.y
            #print'self.t', self.t
            self.X.append(self.y[0])
            self.Y.append(self.y[1])        
            if self.method == 'Euler':
                # y_{n+1} = y_n + k*f(y_n,t_n)
                #f contains the values returned by the func()
                #print'f', f(self.t,self.y) 
                self.y += k[i]*f(self.t,self.y) 
            elif self.method == "RK2":
                y1 = self.y
                y2 = y1 + k[i]*f(self.t,y1)
                self.y += k[i]*(f(self.t,y1)+f(self.t,y2))/2 
            elif self.method == "BDF-1":
                y_get = self.y
                #This method needs a non-linear rootsolver like scipy.optimize.fsolve
                def get(x):
                    out = []
                    out.append(y_get[0]+k[i]*np.pi*np.sin(np.pi*x[0]/2)*np.cos(np.pi*x[1])-x[0])
                    out.append(y_get[1]-k[i]*np.pi/2*np.cos(np.pi*x[0]/2)*np.sin(np.pi*x[1])-x[1])
                    return out
                y_sln = fsolve(get, y_get)
                self.y = y_sln    
            else:
                return "Error:  method ", self.method," is not supported"
            # t_{n+1}
            self.t = self.tn[i+1]
            #print'X',self.X
            #print'Y',self.Y
            self.r_error.append(abs((psi(self.y)-psi_o)/psi_o))
            self.u0.append(self.y[0])                              
            self.u1.append(self.y[1])
            self.psi_func.append(psi(self.y))
        """
        print 'x', self.X
        print 'y', self.Y
        print 'u0', self.u0 
        print 'u1', self.u1  
        print 'tn', self.tn
        print'r_error', len(self.r_error)  """           


# now test the class

#Given a 2-D problem
def func(t,u):
    ##rhs
    x = u[0]
    y = u[1]
    return np.array([np.pi*np.sin(np.pi*x/2)*np.cos(np.pi*y),\
    -np.pi/2*np.cos(np.pi*x/2)*np.sin(np.pi*y)]) 
    
def psi(u):
    #why in streamfunc() stores the value of initial \psi
    x = u[0]
    y = u[1]
    return np.sin(np.pi*x/2)*np.sin(np.pi*y)
    
ini_p = [[0.2, 0.1], [0.2, 0.4]]
n = len(ini_p)
"""Below codes generates two seperate figures
fig1 = plt.figure()
fig2 = plt.figure()
ax = [fig1.gca(projection='3d'),fig2.gca(projection='3d')]
"""
color_line = [['r-', 'r--'],['b:','b-.']]
color_err = ['r-','b--']
n_method = ['Euler','RK2','BDF-1']

error_plot = 1 

for i in xrange(len(n_method)):
#for i in xrange(1)
   ode = stupidode(func)    
   #ode.set_integrator(name, **integrator_params)
   #name: str; integrator_params: additional parameters for the integrator    
   n_max = 6
   n_min = 4
   n = np.linspace(n_min,n_max,n_max-n_min+1)   
   nsteps=[int(1e1**x) for x in n]
   r_err_end = np.zeros([len(ini_p),len(nsteps)]) #stores the error at tmax/nsteps[]
        
   for j in xrange(len(nsteps)):
     ode.set_integrator(n_method[i],nsteps[j])
     #Variables using for plotting
     mpl.rcParams['legend.fontsize'] = 10
     fig = plt.figure()
     ax = 2*[fig.gca(projection='3d')]
     fig_err = plt.figure()
     ax_err = fig_err.add_subplot(1,1,1)  
     legend = []

     for k in xrange(len(ini_p)):
     #for k in xrange(1): 
       #print i
       #ode.set_initial_value(y, t=0.0)
       #Set initial conditions y(t) = y.
       #In this problem, y=e**(-t), so y=1 while t=0
       ode.set_initial_value(np.array(ini_p[k]),0.) 
    
       #ode.integrate(t, step=0, relax=0)
       #Find y=y(t_max=15), set y as an initial condition, and return y.
       #t_max and nsteps determine the time interval k
       ode.integrate(15)  
       err_end = abs((ode.psi_func[-1]-ode.psi_func[0])/ode.psi_func[0])
       r_err_end[k,j] = err_end             
       
       #If J=1, "J={0}".format(J)-->'J=1'; If J=[1, 2, 3], "J={0}".format(J[2])--> 'J=3'
       #If J=[[1,2],[3,4]], "J={0}".format(J[0])--> 'J=[1, 2]'
       #                    "J={0}".format(J[0][1])--> 'J=2'
       #legend.append("Initial Position={0}".format(ini_p[i]))        
       if error_plot == 0:
         ax[k].plot(ode.X, ode.Y, ode.u0, color_line[k][0],label="{0}".format(ini_p[k])+", u")
         ax[k].plot(ode.X, ode.Y, ode.u1, color_line[k][1],label="{0}".format(ini_p[k])+", v")
         ax[k].legend(loc='best')
         ax[k].set_xlabel('x')
         ax[k].set_ylabel('y')
         ax[k].set_title("Velocity field, steps="+r"$1\times 10^{0}$".format(int(n[j]))+"\n"\
                     +str(n_method[i])+" using 2 start points")
         #print 'tn', ode.tn
         #print nsteps + 1
         #print'r_error', len(ode.r_error)
         ax_err.plot(ode.tn[1:],np.log10(ode.r_error[1:]),color_err[k],label='{0}'.format(ini_p[k]))  
         ax_err.legend(loc='best') #legen is the necessary condition for label output   
         ax_err.set_xlabel(r'$tn$')
         ax_err.set_ylabel(r'$log_{10}(err)$')
         ax_err.set_title("Relative error, " + str(n_method[i]) \
                     +", steps="+r"$1\times 10^{0}$".format(int(n[j])))       
         #ax_err.set_xlabel(r'$log_{10}(nsteps)$')
     fig.savefig('Method-{0}-step=1e{1}.png'.format(n_method[i],int(n[j])))   
     fig_err.savefig('Rel_error-{0}-step=1e{1}.png'.format(n_method[i],int(n[j])))            
     
   #print'r_err',r_err_end    p={0:3.3}".format(p[0])
   #Plotting the error at tmax/nsteps[]
   
   fig2_err = plt.figure()
   for k in xrange(len(ini_p)):
     p = []
     p = np.polyfit(np.log(nsteps),np.log(r_err_end[k]),1)
     #print 'p',p
     plt.loglog(nsteps,r_err_end[k],color_err[k],label='{0},p={1:4.4}'.format(ini_p[k],p[0]))   
     plt.legend(loc='best') #legen is the necessary condition for label output   
     plt.xlabel(r'$log_{10}(nsteps)$')
     plt.ylabel(r'$log_{10}(err)$')
     plt.title("Relative error at different tmax, " + str(n_method[i]))    
     plt.grid(True)
   fig2_err.savefig('Rel_error-{0}.png'.format(n_method[i]))  


plt.show(False)
 
    
    