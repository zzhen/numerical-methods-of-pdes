# -*- coding: utf-8 -*-
"""
Created on Sat Nov  2 22:06:33 2013

@author: mspieg

Revised by zzhen
First, this code create a new class类: stupidode;
Following "set_integrator, set_initial_value(), etc." are member functions, which
means the attributs and methods owned by the objects/instances对象 in this class
And then at the lower bottom "ode = stupidode(func)" , this means that stupidode(**) creates 
a object/instance, i.e. "ode";
"""

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt


class stupidode:
    """ simple wrapper class for hand rolled single stepper schemes that mimic
    the scipy ode class:
    
    Usage:  ode = stupidode(f)  # initialize an ode solver with rhs f(t,y)
            ode.set_integrator(method,nsteps)
            ode.set_initial_value(y0,t0)
            ode.integrate(t_max)
    """
    def __init__(self,f): # called when object is constructed
        """Initialize the ode object and set the rhs f
        """
        #self is a class, which owns the value 'f'
        self.f=f
        self.y0 = np.array([0])      
    def set_integrator(self,method,nsteps=1):
        """ set the integration scheme:
        method is the stepper (currently only euler)
        nsteps is the number of fixed sized steps taken over an integration
        """
        self.method = method
        self.nsteps = nsteps
    def set_initial_value(self,y0,t0):
        """ set the initial condition y0,t0
        """
        self.y0 = y0.copy()
        self.t0 = t0
        self.y = y0
        self.t = t0
        
    def integrate(self,t):
        """take nstep steps from t0 to t (ugly!) 
        given by ode.integrate(1.)
        """
        if self.method == 'euler':
            #nsteps is give by ode.set_integrator('euler',nsteps=**)
            nsteps = self.nsteps
            #t is given by ode.integrate(1)
            tn = np.linspace(self.t0,t,nsteps+1)
            k  = np.diff(tn) #because tn has been defined in this method, so 'k'
                             #hasn't been added by 'self.'
            print'k', k
            f = self.f  #set the value of f as it in the __init__(self,f) 
            self.X = []  #Stores values of x axis 
            self.Y = []  #Stores values of y axis
            self.u0 = []  #Stores values of the 1st col in rhs
            self.u1 = []  #Stores values of the 2nd col in rhs  
            print 'nsteps', nsteps
            for i in xrange(nsteps):
                #print'i, k', i, k[i]
                #print'self.y', self.y
                #print'self.t', self.t
                self.X.append(self.y[0])
                self.Y.append(self.y[1])
                #print'f', f(self.t,self.y)    
                # y_{n+1} = y_n + k*f(y_n,t_n)
                #f contains the values returned by the func()
                self.y += k[i]*f(self.t,self.y)               
                # t_{n+1}
                self.t = tn[i+1]
                self.u0.append(self.y[0])                              
                self.u1.append(self.y[1])                                
        else:
            return "Error:  method ",self.method," is not supported"
        """
        print 'x', self.X
        print 'y', self.Y
        print 'u0', self.u0 
        print 'u1', self.u1
        """
# now test the class

#Given a 2-D problem
def func(t,u):
    ##rhs
    x = u[0]
    y = u[1]
    return np.array([np.pi*np.sin(np.pi*x/2)*np.cos(np.pi*y),\
    -np.pi/2*np.cos(np.pi*x/2)*np.sin(np.pi*y)]) 
    
    
ini_p = [[0.2, 0.1], [0.2, 0.4]]

mpl.rcParams['legend.fontsize'] = 10
fig1 = plt.figure()
fig2 = plt.figure()
#ax = fig.gca(projection='3d')
ax = [fig1.gca(projection='3d'),fig2.gca(projection='3d')]
legend = []
#color_line = [['r-', 'b--'],['r+','bx']]
color_line = [['r-', 'b--'],['r-', 'b--']]
for i in xrange(len(ini_p)):
#for i in xrange(1):
    ode = stupidode(func)
    
    #ode.set_integrator(name, **integrator_params)
    #name: str; integrator_params: additional parameters for the integrator
    ode.set_integrator('euler',nsteps=10000)
    
    #ode.set_initial_value(y, t=0.0)
    #Set initial conditions y(t) = y.
    #In this problem, y=e**(-t), so y=1 while t=0
    ode.set_initial_value(np.array(ini_p[i]),0.) 
    
    #ode.integrate(t, step=0, relax=0)
    #Find y=y(t_max=15), set y as an initial condition, and return y.
    #t_max and nsteps determine the time interval k
    ode.integrate(15)
    
    #If J=1, "J={0}".format(J)-->'J=1'; If J=[1, 2, 3], "J={0}".format(J[2])--> 'J=3'
    #If J=[[1,2],[3,4]], "J={0}".format(J[0])--> 'J=[1, 2]'
    #                    "J={0}".format(J[0][1])--> 'J=2'
    #legend.append("Initial Position={0}".format(ini_p[i]))    
    
    ax[i].plot(ode.X, ode.Y, ode.u0, color_line[i][0],label="u")
    ax[i].plot(ode.X, ode.Y, ode.u1, color_line[i][1],label="v")
    ax[i].legend(loc=0)
    ax[i].set_xlabel('x')
    ax[i].set_ylabel('y')
    ax[i].set_title("Initial Position="+r"${0}$".format(ini_p[i]))


plt.show(False)

    
    