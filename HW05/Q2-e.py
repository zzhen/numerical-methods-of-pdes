# -*- coding: utf-8 -*-
"""
Demonstration code for solving stiff systems of equations in scipy

Created on Wed Nov 14 22:27:41 2012

@author: mspieg
@revised by: zhen zhao

"""
from scipy.integrate import ode
from scipy.linalg import expm
from scipy import dot
from numpy import *
import pylab as pl
import numpy as np
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D

#set line parameters
pl.rc('lines',linewidth=2)
pl.rc('font',weight='bold')

## the RHS function to be passed to ODExx

def SF(t,u):  #Stream Function
    f = zeros(u.shape)
    x = u[0]
    y = u[1]
    f[0] = np.pi*np.sin(np.pi*x/2)*np.cos(np.pi*y)
    f[1] = -np.pi/2*np.cos(np.pi*x/2)*np.sin(np.pi*y)
    return f
    
def jacobian(t,u):
    A = zeros((len(u),len(u)))
    x = u[0]
    y = u[1]
    A[0,0] = np.pi*np.pi/2*np.cos(np.pi*x/2)*np.cos(np.pi*y)
    A[0,1] = -np.pi*np.pi*np.sin(np.pi*x/2)*np.cos(np.pi*y)
    A[1,0] = (np.pi/2)*(np.pi/2)*np.sin(np.pi*x/2)*np.sin(np.pi*y)
    A[1,1] = -np.pi*np.pi/2*np.cos(np.pi*x/2)*np.cos(np.pi*y)
    return A
        
        

def runode(ode,u0,t0,tmax,nsteps,showoutput=False):
    """  wrapper function to call ODE for fixed number of steps and return arrays
    """
    t = linspace(t0,tmax,nsteps)
    u = zeros((nsteps,len(u0))) #u0 .eq. ini_p[i], initial position
    error = zeros(nsteps)
    ode.set_initial_value(u0,t0)
    initial = psi(u0)
    position = zeros((nsteps,len(u0))) #stores values of positions
    # hugely wasteful but evaluate the odeintegrator at points t
    for k in xrange(len(t)):
        ode.set_initial_value(ode.y,ode.t)
        ode.integrate(t[k])
        u[k,:] = ode.y
        position[k,:] = ode.y
        #print't',t[k]
        #print'k,u',k, ode.y
        error[k] = abs((psi(ode.y)-initial)/initial)
        if showoutput:
            print t[k], u[k,:]

    return t,u,position,error   

def psi(u):
    #u in psi() stores the value of initial \psi
    x = u[0]
    y = u[1]
    return np.sin(np.pi*x/2)*np.sin(np.pi*y)  
    
    
def psi_plot(i,u,position,n_method,ini_p,n_orbit,ax):
    color_line = [['r-', 'r--'],['b:','b-.']]
    ax.plot(position[:-1,0], position[:-1,1], u[1:,0], color_line[i][0],label="{0}".format(ini_p[i])+", u")
    ax.plot(position[:-1,0], position[:-1,1], u[1:,1], color_line[i][1],label="{0}".format(ini_p[i])+", v")
    ax.legend(loc='best')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title("Velocity field, steps="+r"$1\times 10^{0}$".format(n_orbit)+"\n"\
                     +str(n_method)+", using 2 start points")

def err_plot(i,t,err,n_method,ini_p,n_orbit,ax_err):
    ax_err.plot(t,np.log10(err),color_err[i],label='{0}'.format(ini_p[i]))  
    ax_err.legend(loc='best') #legen is the necessary condition for label output   
    ax_err.set_xlabel(r'$tn$')
    ax_err.set_ylabel(r'$log_{10}(err)$')
    ax_err.set_title("Relative error, " + str(n_method) \
                     +", steps="+r"$1\times 10^{0}$".format(n_orbit))       



t0 = 0.
tmax = 15.
ini_p = [[0.2, 0.1], [0.2, 0.4]]  #initial position
n = len(ini_p)
n_orbit = 5
orbits = 10**n_orbit
# set options
RelTol = 1.e-6
AbsTol = 1.e-8

emp1 = []
emp2 = []
U = [emp1,emp2]
P = [emp2,emp2]
ERR = [emp1,emp2]
T = [emp1,emp2]

mpl.rcParams['legend.fontsize'] = 10
fig = plt.figure()
ax = fig.gca(projection='3d')

fig_err = plt.figure()
ax_err = fig_err.add_subplot(1,1,1)  

color_err = ['r-','b--']
n_method = 'dopri5 & vode'

for i in xrange(n):
    # now use a proper stiff integrator
    odebdf= ode(SF,jacobian)
    #odebdf.set_integrator('dopri5',atol=AbsTol, rtol=RelTol,nsteps=10000)
    odebdf.set_integrator('vode',method='bdf',with_jacobian=True,atol=AbsTol, rtol=RelTol,nsteps=orbits)
    odebdf.set_initial_value(ini_p[i],t0)
    t,u,position,err = runode(odebdf,ini_p[i],t0,tmax,orbits)
    psi_plot(i,u,position,n_method,ini_p,n_orbit,ax)
    err_plot(i,t,err,n_method,ini_p,n_orbit,ax_err)


fig.savefig('Method-{0}-step=1e{1}.png'.format(n_method,n_orbit))   
fig_err.savefig('Rel_error-{0}-step=1e{1}.png'.format(n_method,n_orbit)) 

    

"""   
pl.figure(1)
legend = []
for i in xrange(n):
    scatterode(T[i],U[i])
    #print'T', T[i]
    #print'U', U[i]
    legend.append("Start Point=("+str(ini_p[i][0])+" , "+str(ini_p[i][1])+")")
pl.legend(legend,loc='best')

pl.figure(2)
legend = []
for i in xrange(n):
    pl.plot(T[i],log10(ERR[i]))
    legend.append("Start Point=("+str(ini_p[i][0])+" , "+str(ini_p[i][1])+")")
pl.legend(legend,loc='best')
    
pl.show(block=False)
"""
