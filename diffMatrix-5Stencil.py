# -*- coding: utf-8 -*-
"""
diffMatrix:  example program to set up sparse differentiation matrix
Created on Tue Sep 18 01:27:13 2012

@author: mspieg
"""

import scipy.sparse as sp
import numpy as np
import pylab
import math

from fdcoeffF import fdcoeffF
from numpy import linalg as LA

def setD(k,x):
    """ 
    example function for setting k'th order sparse differentiation matrix over 
    arbitrary mesh x with a 3 point stencil
    
    input:
        k = degree of derivative <= 2
        x = numpy array of coordinates >=3 in length
    returns:
        D sparse differention matric
    """
    
    assert(k < 3) # check to make sure k < 3
    assert(len(x) > 2)
    
    N = len(x)
    # initialize a sparse NxN matrix in "lil" (linked list) format
    D = sp.lil_matrix((N,N))
    # assign the one-sided k'th derivative at x[0]
    D[0,0:5] = fdcoeffF(k,x[0],x[0:5])
    # assign centered k'th ordered derivatives in the interior
    for i in xrange(1,N-3):
        D[i,i-1:i+4] = fdcoeffF(k,x[i],x[i-1:i+4])
    # assign one sided k'th derivative at end point x[-1]
    D[N-3,-5:] = fdcoeffF(k,x[N-3],x[-5:])    
    D[N-2,-5:] = fdcoeffF(k,x[N-2],x[-5:])    
    D[N-1,-5:] = fdcoeffF(k,x[N-1],x[-5:])
    
    # convert to csr (compressed row storage) and return
    return D.tocsr()


# choose a simple quadratic function
def df(k,x):
    """
    Set k=1 first derivatives and k=2 second derivatives of original function f = x^2 + sin(4*pi*x)
    """
    df1 = [0] * len(x)
    df2 = [0] * len(x)
#    print "k", k
    if k == 1 :
       df1 = 2*x + 4 * math.pi * np.cos(4*math.pi*x)
#       print "checked-df1",df1
       return df1              
    elif k == 2 :
       df2 = 2 - 16 * (math.pi**2) * np.sin(4*math.pi*x)       
#       print "checked-df2",df2       
       return df2
    else :
       print "The order of derivs beyond computation"




def err(k,x,f):
# Calculation of the 2-Norm of the absolute mesh error
    y = f(x)
#    print "checked-y",y
    err1 = [0] * len(x)
    err2 = [0] * len(x)
    if k == 1 :
       err1 = setD(1,x) * y - df(1,x)
#       print "checked-err1",err1
       return err1
    if k == 2 :
       err2 = setD(2,x) * y - df(2,x)
#       print "checked-err2",err2
       return err2



# quicky test program
def plotDf(x,h,eh1,eh2,title=None):
#    print "eh1",eh1
#    print "eh2",eh2
    
    # show sparsity pattern of D1
    pylab.figure()
    pylab.spy(setD(1,x))
    pylab.title("Sparsity pattern of D1")    
    
    
# plot a function and it's derivatives
    pylab.figure()
#    pylab.plot(h,np.log10(eh1),h,np.log10(eh2))
    pylab.plot(np.log10(h),np.log10(eh1),'--',np.log10(h),np.log10(eh2))
    pylab.xlabel('log10(h)')
    pylab.ylabel('log10(Eh)')
    pylab.xlim(-3.25, -0.75)# set axis limits
    pylab.ylim(-8.5, 2.5)
    pylab.plot(h,eh1,h,eh2)
    pylab.legend(['Err-1','Err-2'],loc="best")
    if title:
        pylab.title(title)
    pylab.show(block=False)


def main():
    xM = 1 # Maxmum value of x
    xm = 0 # Minimum value of x
    # set numpy grid array to be evenly spaced
    nc = 8 # Number of calculations
    xdiv = [0.0]*nc # This array determines the mesh dimensions of each calculation
    h = [0.0]*nc # Mesh length
    eh1 = [0.0]*nc # Storing error of per calculation
    eh2 = [0.0]*nc # Storing error of per calculation

    def f(x):
        return x**2 + np.sin(4*pi*x)


    for i in range(3,11):
      xdiv[i-3] = 2**i   # i begins from 0
#      print xdiv
      h[i-3] = 2**(-i)
      ndiv = xdiv[i-3]   # magnitude of mesh
#      print ndiv
      x = np.linspace(xm,xM,ndiv+1)
#      print "x",x
      eh1[i-3] = h[i-3]**0.5 * LA.norm(err(1,x,f),2)
      eh2[i-3] = h[i-3]**0.5 * LA.norm(err(2,x,f),2)

#    print "eh1",eh1
#     print "eh2",eh2
    plotDf(x,h,eh1,eh2,"Err-h")



if __name__ == "__main__":
    main()
