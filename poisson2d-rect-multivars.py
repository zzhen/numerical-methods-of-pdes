"""
poisson.py  -- solve the Poisson problem u_{xx} + u_{yy} = f(x,y)
                on [a,b] x [a,b].
                
                using python sparse linear algebra

     The 5-point Laplacian is used at interior grid points.
     This system of equations is then solved using scipy.sparse.linalg spsolve
     which defaults to umfpack

     code modified from poisson.py from 
     http://www.amath.washington.edu/~rjl/fdmbook/chapter3  (2007)
"""

import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pylab
from mpl_toolkits.mplot3d import Axes3D


def u_exact(x,y):
    """ 
    exact solution for this problem
    exp(x+y/2)
    """
    X,Y = np.meshgrid(x,y)
    u_exact = np.zeros(X.shape)  
    
    for i in range(0,min(len(x),len(y))):
        u_exact[i,:] = np.exp(x+y[i]/2)    
#    print('u_exact'),u_exact
        
    return u_exact
    
def usoln(x,y,nx,ny,h):
    m = min(len(x),len(y)) - 2  # number of interior points in each direction

    X,Y = np.meshgrid(x,y)     # 2d arrays of x,y values
    # X = X.T                    # Get right index of (i,j), transpose so that X(i,j),Y(i,j) are
    # Y = Y.T                    # coordinates of (i,j) point

    Xint = X[1:-1,1:-1]        # if X is N*N Dim, then this means elements between [1,N-1]*[1,N-1]
    Yint = Y[1:-1,1:-1]        # so they are interior points

    def f(x,y):
        return np.zeros(x.shape)

    #evaluate f at interior points for right hand side
    rhs = f(Xint,Yint)         # only interior points, (N-2)*(N-2) Dim
               # rhs is modified below for boundary conditions.
    #AA = f(Xint,Yint)  #just use for display A
    
    # set boundary conditions around edges of usoln array:
    usoln = (np.zeros(X.shape))  # here we just zero everything  
                           # This sets full array, but only boundary values
                           # are used below.  For a problem where utrue
                           # is not known, would have to set each edge of
                           # usoln to the desired Dirichlet boundary values.


    # adjust the rhs to include boundary terms: 
    # only interior points, (m)*(2m) Dim
    for j in range(0,m):
      rhs[j,:] = 5./4*np.exp(x[1:-1] + y[j+1]/2)
    
    # define boundary conditions
    g = u_exact(x,y)  
    
    #print('rhs'),rhs
    # Apply boundary conditions into rhs
    rhs[0,:] -= g[0,1:-1] / h**2  #Bottom line
    rhs[-1,:] -= g[-1,1:-1] / h**2  #Top line
    rhs[:,0] -= g[1:-1,0] / h**2  #Left line
    rhs[:,-1] -= g[1:-1,-1] / h**2  #Right line



    # convert the 2d grid function rhs into a column vector for rhs of system:
    F = rhs.reshape((m*(2*m+1),1))  #still just contain interior points



    #print('F'),F

    # form matrix A:
    # these sparse matrix can be displayed use **.todense()
    mmax = max(nx,ny) - 2
    mmin = min(nx,ny) - 2
    Imin = sp.eye(mmin,mmin)  #all 1 in the diagonal elements
    e = np.ones(mmax)
    T = sp.spdiags([e,-4.*e,e],[-1,0,1],mmax,mmax) #the elements of three main diagonal lines
    S = sp.spdiags([e,e],[-1,1],mmin,mmin)
    Imax = sp.eye(mmax,mmax)
    #S = sp.spdiags([e],[0],mmin,mmin)

    #kron means Kronecker product AXB
    A = (sp.kron(Imin,T) + sp.kron(S,Imax)) / h**2
    A = A.tocsr()

    #since A is a matrix, and couldn't be displayed in Variable Explorer
    #it need to be changed to "list"
    #AA = (A.todense())


    show_matrix = True
    if (show_matrix):
        pylab.spy(A,marker='.')

    # Solve the linear system:
    uvec = spsolve(A, F)

    # reshape vector solution uvec as a grid function and
    # insert this interior solution into usoln for plotting purposes:
    # (recall boundary conditions in usoln are already set)

    usoln[1:-1, 1:-1] = uvec.reshape( (ny-2,nx-2) )
    #Assign boundary values to usoln
    usoln[:,0] = g[:,0]  #Bottom line
    usoln[:,-1] = g[:,-1]  #Top line
    usoln[0,:] = g[0,:]  #Leftmost line
    usoln[-1,:] = g[-1,:]  #Rightmost line

    # using Linf norm of spectral solution good to 10 significant digits
    umax_true = 0.07367135328
    umax = usoln.max()  #get the maxmium elements in the matrix
    abs_err = abs(umax - umax_true)
    rel_err = abs_err/umax_true
    print "m = {0}".format(m)
    print "||u||_inf = {0}, ||u_true||_inf={1}".format(umax,umax_true)
    print "Absolute error = {0:10.3e}, relative error = {1:10.3e}".format(abs_err,rel_err)

    #show_result = False
    show_result = True
    if show_result:
    # plot results:
        pylab.figure()
        ax = Axes3D(pylab.gcf())
        ax.plot_surface(X,Y,usoln, rstride=1, cstride=1, cmap=pylab.cm.jet)
        ax.set_xlabel('t')
        ax.set_ylabel('x')
        ax.set_zlabel('u')
        #pylab.axis([a, b, a, b])
        #pylab.daspect([1 1 1])
        pylab.title('Surface plot of computed solution')

    pylab.show(block=False)
    #pylab.show(block=True)
    return usoln


    
def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """
    
    return np.sqrt(h)*np.linalg.norm(f, 2)

def plotconvergence(N_ar,abs_err_ar,rel_err_ar):
    """ make pretty convergence plots
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    h = 1./np.array(N_ar)
    abs_err = np.array(abs_err_ar)
    rel_err = np.array(rel_err_ar)
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    p = np.polyfit(np.log(h),np.log(abs_err),1)
    print('p'),p
    pylab.figure()
    pylab.loglog(h,abs_err,'bo-',h,rel_err,'ro-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.3}".format(p[0]))
    pylab.legend(["abs_err","rel_error","best-fit p"],loc="best")
    pylab.grid()
    pylab.show(block=False)
    #pp.savefig()




def main():
    # set number of mesh intervals (mesh points is N+1)
    a = 0.
    b = 2.
    ab = b - a
    c = 0.
    d = 1.
    cd = d - c
#    N_ar = [16,32,64,128]
    N_ar = [16,32,64,128,256] 
#    N_ar = [4]  # Used for testing
    # initialize lists for storing output
    abs_err_ar = []
    rel_err_ar = []
    for N in N_ar:
        # set numpy grid array to be evenly spaced    
        h = min(ab,cd)/(N + 1)
        nx = 2*(N + 1) + 1
        ny = (nx + 1)/2         # hx = hy    
        x = np.linspace(a,b,nx)   # grid points x including boundaries
        y = np.linspace(c,d,ny)   # grid points y including boundaries
        
        u_true = u_exact(x,y)        
        u_solve = usoln(x,y,nx,ny,h)     
        #print('u_true'),u_true
        #print('u_solve'),u_solve
        
        # calculate error and errornorm        
        err = u_solve - u_true
        abs_err = grid_norm2(err,h)
        rel_err = abs_err/grid_norm2(u_solve,h)
        
       
        print 'N=', N, 'abs_err=', abs_err, 'rel_err=',rel_err
        # collect results for later plotting 
        abs_err_ar.append(abs_err)
        rel_err_ar.append(rel_err)
        
    plotconvergence(N_ar,abs_err_ar,rel_err_ar)
    
if __name__ == "__main__":
    main()
